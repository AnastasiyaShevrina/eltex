

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
r
struct kadr{
    char fname[50];
    int bd, number,oklad;
};
typedef struct kadr kadrs;
 
void readkadr(kadrs *ks){
    printf("Введите Фамилию:");
    scanf("%s", ks->fname);
    printf("Введите год рождения:");
    scanf("%d", &ks->bd);
    printf("Введите номер отдела:");
	scanf("%d", &ks->number);
	printf("Введите размер оклада:");
	scanf("%d", &ks->oklad);
}

static int cmp(const void *p1, const void *p2){
    kadrs * ks1 = *(kadrs**)p1;
    kadrs * ks2 = *(kadrs**)p2;
    return ks1->bd - ks2->bd;
}
 
int main(int argc, char **argv){
    int count;
    int i;
    printf("Введите кол-во сотрудников:");
    scanf("%d", &count);
    
    kadrs** ks = (kadrs**)malloc(sizeof(kadrs**)*count);
    for (i = 0; i < count ; i++){
        ks[i] = (kadrs*) malloc (sizeof(kadrs));
        readkadr(ks[i]);
    } 
    qsort(ks, count, sizeof(kadrs*), cmp);
   for (i = 0; i < count ; i++)
    printf("%s  - %d \n", ks[i]->fname, ks[i]->bd);
    for (i = 0; i < count; i++)
    {
        free(ks[i]);
    }
    free(ks);
    return 0;
}

