

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cmpstringp(const void *p1, const void *p2) {
	const char * a = *(const char**) p1;
	const char * b = *(const char**) p2;
	return strcmp(a,b);
}

int main(int argc, char **argv)
{	
    char ** s1;
    char    buf[80]; // буфер для хранения строки из gets
	int     n,i;
    int     len; // размер строки в буфере

	printf ("Введите количество строк\n");
	scanf ("%d\n",&n);
    /* Выделяем память под n указателей на char (каждый указатель будет хранить указатель на строку) */
    s1 = (char **) malloc(sizeof(char *) * n);
	for (i=0;i<n;i++) {        
	    gets (buf);
        /* узнаем длину считываемой строки */
        len = strlen(buf);
        /* выделяем память под строку */
        s1[i] = (char *) malloc (sizeof(char) * len);
        /* копируем строку в массив */
        memcpy(s1[i], buf, (size_t)len);
	}
	printf("Для отладки sort\n");
    
    /* передаем указатель на массив указателей, размер массива (n) и размер элементов массива (размер элементов массива - это размер указателя на char) */
	qsort(s1, n, sizeof(char *),cmpstringp);
	
    printf("Point\n");
	for (i=0;i<n;i++) {
		printf ("%d: %s(%d)\n", i, s1[i], strlen(s1[i]));
	}

    /* освобождаем выделенную память под строки */
    for(i = 0; i < n; i++)
        free(s1[i]);

    /* освобождаем память для массива указателей */
    free(s1);    

	return 0;
}

